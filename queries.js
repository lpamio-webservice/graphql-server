const { db } = require("./db");

const {
  GraphQLScalarType
} = require('graphql');

exports.typeDefs = `

scalar Date

type Movie {
  id: ID!,
  title: String!,
  year: Int,
  actors: [Actor]
}

type Actor {
  id: ID!,
  name: String!,
  dob: Date,
  movies: [Movie]
}

type Query {
  version: String,
  movie( id: String!): Movie,
  movies: [Movie],
  actor( id: String!): Actor,
  actors (nb: Int): [Actor!],
}

type Mutation {
  addActor (firstname: String, lastname: String, dob: Date): Actor
}
`;

const DateType = new GraphQLScalarType({
  name: 'Date',
  description: 'Description of my custom scalar type',
  serialize(value) {
    var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
    return value.toLocaleDateString("fr-FR", options);
  },
  parseValue(value) {
    let result;
    result = "3";
    return result;
  },
  parseLiteral(ast) {
    return new Date();
  }
});

exports.resolvers = {

  Date: DateType,

  Query: {
    version: () => 'Cinema 1.0',

    actor: (obj, args) => {

      const query = `SELECT * FROM cinema.actor WHERE id=$1`;
      const values = [args.id];

      return db.one(query, values)
        .then(res => res)
        .catch(err => err);
    },

    actors: (obj, args) => {

      let query = `SELECT * from cinema.actor ORDER BY movie_count desc`;
      
      if (args && args.nb) {
        query += ` LIMIT ${args.nb}`
      }

      return db
        .manyOrNone(query)
        .then(res => res)
        .catch(err => err);
    }
  },

  Actor: {
    movies: (obj, args) => {

      const query = `SELECT m.id, m.title, m.year, c.alias FROM cinema.castandcrew c INNER JOIN cinema.movie m
      ON (c.movie = m.id) WHERE c.person=$1 ANd role = 'actor'
      ORDER BY m.year`;
      const values = [args.id];

      return db.manyOrNone(query, values)
        .then(res => res)
        .catch(err => err);
    }
  },

  Mutation: {
    addActor: (obj, args) => {

      const query = `INSERT INTO cinema.person (firstname, lastname, dob) VALUES ($1, $2, $3) RETURNING *`;
      const values = [args.firstname, args.lastname, args.dob];

      return db.one(query, values)
        .then(res => res)
        .catch(err => err);
    }
  }

};
