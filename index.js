"use strict";

require('dotenv').config();
const port = process.env.PORT || 3200;

const app = require('express')();

const { graphqlHTTP } = require("express-graphql");
const { makeExecutableSchema } = require("graphql-tools");
const { typeDefs, resolvers } = require("./queries");

const schema = makeExecutableSchema({ typeDefs: typeDefs, resolvers: resolvers });

app.use('/graphql', graphqlHTTP({
  schema: schema,
  graphiql: true
}));

app.listen(port, () => {
  console.log(`Server is running on ${port} port.`);
})
